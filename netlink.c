#include <stdio.h>
#include <sys/socket.h>
#include <linux/rtnetlink.h>
#include <arpa/inet.h>
#include <errno.h>
#include <net/if.h>
#include <sys/types.h>
// #include <memory.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>

int need_stope_flag = 0;

typedef int (*nl_handler_t)(struct nlmsghdr *h);

nl_handler_t handlers[__RTM_MAX];

void handler (int signum)
{
	need_stope_flag = 1;
}

void parseRtattr(struct rtattr *tb[], int max, struct rtattr *rta, int len)
{
	memset(tb, 0, sizeof(struct rtattr *) * (max + 1));

	while(RTA_OK(rta, len))
	{
		if (rta -> rta_type <= max)
			tb[rta -> rta_type] = rta;
		rta = RTA_NEXT(rta, len);
	}

}

int RtmNewRoutCB(struct nlmsghdr *h)
{
	char rta_dst[256];
	char rta_src[256];
	char rta_gw[256];
	int rta_metrics;
	struct rtattr *td[RTA_MAX + 1];
	struct rtmsg  *ifrt;

	ifrt = (struct rtmsg *) NLMSG_DATA(h);
	
	parseRtattr(td, RTA_MAX, RTM_RTA(ifrt), h -> nlmsg_len);
	
	printf("New rout was detected\n");
	if (td[RTA_DST])
	{
		inet_ntop(AF_INET, RTA_DATA(td[RTA_DST]), rta_dst, sizeof(rta_dst));
		printf("Addr DST: %s\n", rta_dst);
	}
	if (td[RTA_SRC])
	{
		inet_ntop(AF_INET, RTA_DATA(td[RTA_SRC]), rta_src, sizeof(rta_src));
		printf("Addr SRC: %s\n", rta_src);
	}
	if (td[RTA_GATEWAY])
	{
		inet_ntop(AF_INET, RTA_DATA(td[RTA_GATEWAY]), rta_gw, sizeof(rta_gw));
		printf("Addr GW: %s\n", rta_gw);
	}
	if (td[RTA_METRICS])
	{
		rta_metrics = *(int *)RTA_DATA(td[RTA_METRICS]);
		printf("Metrics: %d\n", rta_metrics);
	}
	else
		printf("Metrics: %d\n", 0);
	printf("\n");

	return 0;
}

int RtmDelRoutCB(struct nlmsghdr *h)
{
	char rta_dst[256];
	char rta_src[256];
	char rta_gw[256];
	int rta_metrics;
	struct rtattr *td[RTA_MAX + 1];
	struct rtmsg  *ifrt;

	ifrt = (struct rtmsg *) NLMSG_DATA(h);
	
	parseRtattr(td, RTA_MAX, RTM_RTA(ifrt), h -> nlmsg_len);
	
	printf("The rout was deleted\n");
	if (td[RTA_DST])
	{
		inet_ntop(AF_INET, RTA_DATA(td[RTA_DST]), rta_dst, sizeof(rta_dst));
		printf("Addr DST: %s\n", rta_dst);
	}
	if (td[RTA_SRC])
	{
		inet_ntop(AF_INET, RTA_DATA(td[RTA_SRC]), rta_src, sizeof(rta_src));
		printf("Addr SRC: %s\n", rta_src);
	}
	if (td[RTA_GATEWAY])
	{
		inet_ntop(AF_INET, RTA_DATA(td[RTA_GATEWAY]), rta_gw, sizeof(rta_gw));
		printf("Addr GW: %s\n", rta_gw);
	}
	if (td[RTA_METRICS])
	{
		rta_metrics = *(int *)RTA_DATA(td[RTA_METRICS]);
		printf("Metrics: %d\n", rta_metrics);
	}
	else
		printf("Metrics: %d\n", 0);
	printf("\n");

	return 0;
}

int RtmNewLinkCB(struct nlmsghdr *h)
{
	char rta_dst[256];
	char *ifName;
	char *ifUpp;
	char *ifRunn;
	struct rtattr *td[IFLA_MAX + 1];
	struct ifinfomsg  *ifrt;

	ifrt = (struct ifinfomsg *) NLMSG_DATA(h);
	
	parseRtattr(td, RTA_MAX, IFLA_RTA(ifrt), h -> nlmsg_len);
	
	if (td[IFLA_IFNAME])
		ifName = (char *)RTA_DATA(td[IFLA_IFNAME]);

	if (ifrt -> ifi_flags & IFF_UP)
		ifUpp = (char *)"UP";
	else
		ifUpp = (char *)"DOWN";

	if (ifrt -> ifi_flags & IFF_RUNNING)
		ifUpp = (char *)"RUNNING";
	else
		ifUpp = (char *)"NOT RUNNING";

	printf("New link %s detected, state %s %s\n", ifName, ifUpp, ifRunn);

	return 0;
}

int RtmDelLinkCB(struct nlmsghdr *h)
{
	char *ifName;
	char *ifUpp;
	char *ifRunn;
	struct rtattr *td[IFLA_MAX + 1];
	struct ifinfomsg  *ifrt;

	ifrt = (struct ifinfomsg *) NLMSG_DATA(h);
	
	parseRtattr(td, RTA_MAX, IFLA_RTA(ifrt), h -> nlmsg_len);
	
	if (td[IFLA_IFNAME])
		ifName = (char *)RTA_DATA(td[IFLA_IFNAME]);


	printf("Link %s deleted\n", ifName);

	return 0;
}

int RtmNewAddrCB(struct nlmsghdr *h)
{
	char addr[256];
	char *ifName;
	struct rtattr *td[IFLA_MAX + 1];
	struct ifaddrmsg  *ifrt;

	ifrt = (struct ifaddrmsg *) NLMSG_DATA(h);
	
	parseRtattr(td, RTA_MAX, IFA_RTA(ifrt), h -> nlmsg_len);
	
	if (td[IFA_LABEL])
		ifName = (char *)RTA_DATA(td[IFA_LABEL]);

	if (td[IFA_LOCAL])
		inet_ntop(AF_INET, RTA_DATA(td[IFA_LOCAL]), addr, sizeof(addr));

	printf("New address was added %s: %s\n", ifName, addr);

	return 0;
}

int RtmDelAddrCB(struct nlmsghdr *h)
{
	char addr[256];
	char *ifName;
	struct rtattr *td[IFLA_MAX + 1];
	struct ifaddrmsg  *ifrt;

	ifrt = (struct ifaddrmsg *) NLMSG_DATA(h);
	
	parseRtattr(td, RTA_MAX, IFA_RTA(ifrt), h -> nlmsg_len);
	
	if (td[IFA_LABEL])
		ifName = (char *)RTA_DATA(td[IFA_LABEL]);

	printf("Address was deleted %s\n", ifName);

	return 0;
}

int main(void)
{
	signal(SIGINT, handler);
	signal(SIGTERM, handler);

	handlers[RTM_NEWROUTE] = RtmNewRoutCB;
	handlers[RTM_DELROUTE] = RtmDelRoutCB;
	handlers[RTM_NEWADDR] = RtmNewAddrCB;
	handlers[RTM_DELADDR] = RtmDelAddrCB;
	handlers[RTM_NEWLINK] = RtmNewLinkCB;
	handlers[RTM_DELLINK] = RtmDelLinkCB;

	int fd = socket(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE);
	if (fd < 0)
	{
		printf("socket err\n");
		exit(1);
	}

	struct sockaddr_nl local;
	char buf[8192];
	struct iovec iov;
	iov.iov_base = buf;
	iov.iov_len = sizeof(buf);

	memset(&local, 0, sizeof(local));

	local.nl_family = AF_NETLINK;
	local.nl_groups = RTMGRP_LINK | RTMGRP_IPV4_IFADDR | RTMGRP_IPV4_ROUTE;
 	local.nl_pid = getpid();

 	struct msghdr msg;
 	msg.msg_name = &local;
 	msg.msg_namelen = sizeof(local);
 	msg.msg_iov = &iov;
 	msg.msg_iovlen = 1;

 	if (bind(fd, (struct sockaddr*)&local, sizeof(local)) < 0)
 	{
 		printf("bind err\n");
 		exit(1);
 	}

 	while(1)
 	{
 		if (need_stope_flag)
	 		break;

 		ssize_t status = recvmsg(fd, &msg, MSG_DONTWAIT);

 		if(status < 0)
 		{
 			if (errno == EINTR || errno == EAGAIN)
 			{
 				usleep(250000);
 				continue;
 			}

 			printf("recv err\n");
 			continue;
 	
 		}

		if (msg.msg_namelen != sizeof(local))
	 	{
	 		printf("err len\n");
	 		continue;
	 	}

	 	struct nlmsghdr *h = (struct nlmsghdr *)buf;
	 	while(status >= (ssize_t)sizeof(*h))
	 	{
	 		int len = h -> nlmsg_len;
	 		int l = len - sizeof(*h);

	 		if ((l < 0) || (len > status))
	 		{
	 			printf("err len mes\n");
	 			continue;
	 		}

	 		if (handlers[h -> nlmsg_type] && handlers[h -> nlmsg_type](h))
	 		{
	 			printf("err call back func\n");
	 			exit(1);
	 		}

	 		status -= NLMSG_ALIGN(len);
	 		h = (struct nlmsghdr *)((char *)h + NLMSG_ALIGN(len));
	 	}

	 	usleep(250000);

	}

	close(fd);

	return 0;
}


